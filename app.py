def compare(list1, list2):
    diff_list = []
    sep_list1 = list1.split(' ')
    sep_list2 = list2.split(' ')
    for elem in sep_list2:
        #In the future we should keep track
        #Of Index so we dont double loop
        if elem not in sep_list1:
            diff_list.append(elem)
    for elem in sep_list1:
        if elem not in sep_list2:
            diff_list.append(elem)

    return diff_list

input_list1 = input("Enter the first list: ")
input_list2 = input("Enter the second list: ")
different_elements = compare(input_list1, input_list2)
print(different_elements)